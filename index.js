

// var path = require('path');
//setup css-modules-require-hook
//https://github.com/css-modules/css-modules-require-hook
require('./cmrh.conf');

//load .env
require('dotenv').load();


if(process.env.NODE_ENV === 'development'){

    //enable on-the-fly es6 and react code transpilation
    require("babel-register");

    //init server, then init webpack-dev-server
    require('./src/server').default( () =>  require('./webpack/server').default() );
}

if(process.env.NODE_ENV === 'production') {
    require('./dist/server').default();
}
