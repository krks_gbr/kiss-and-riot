


import React from 'react';
import style from './style.scss';
import SubHeader from '../../components/sub-header';
import _ from 'lodash';
import {Link} from 'react-router';



export default ({students, teachers, projects}) => {


    students = _.sortBy(students, student => student.name.first);
    students = students.map(student => {

        let project = _.find(projects, project => project.student._id == student._id);
        let studentName = student.name.first + " " + student.name.last;
        let node = project ?
                                <li key={student._id}>
                                    <Link to={"/projects/" + project.slug }>
                                        {studentName}
                                    </Link>
                                </li>
                                :
                                <li key={student._id}>{studentName}</li>
            ;

        return node;

    });

    teachers = _.sortBy(teachers, teacher => teacher.name.first);
    teachers = teachers.map(teacher => {

        let name = teacher.name.first + " " + teacher.name.last;
        return teacher.webpage === "" ?
                        <li key={teacher._id}>{name}</li>
                        :
                        <li><a href={"//"+teacher.webpage} target="_blank">{name}</a></li>

    });


    return (
        <div className={style.participants}>
            <SubHeader title="Participants"/>
            <div className={style.container}>
                <div className={style.col}>
                    <div className={style.colHeader}>Students</div>
                    <div className={style.list}>
                        <ul>{students}</ul>
                    </div>
                </div>
                <div className={style.col}>
                    <div className={style.colHeader}>Mentors</div>
                    <div className={style.list}>
                        <ul>{teachers}</ul>
                    </div>
                </div>
            </div>
        </div>
    )




}
