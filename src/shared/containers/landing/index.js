




import React from 'react';
import style from './style.scss';
import SVGComponent from '../../components/svg-component';
import {bg_red, bg_blue, bg_green, bg_yellow} from '../../style/colors.scss';
import classNames from 'classnames';
import Gallery from '../project-gallery';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Ticker from './ticker';


export default ({content}) => {


    let crowns =  [             <SVGComponent key="black"
                                              className={style.tickerItemSeparator}
                                              svgString={process.env.IS_BROWSER ? require('./logos/crown_black.svg') : ""}/>,
                                <SVGComponent key={"white"}
                                              className={style.tickerItemSeparator}
                                              svgString={process.env.IS_BROWSER ? require('./logos/crown_white.svg') : ""}/>
        ];
    
    
    let studentNameSeparator = crowns.pop();
    let projectTitleSeparator = crowns.pop();
    
    let introText = <div key="intro"  className={style.tickerContent}>
                        <div className={style.tickerItem}>
                            {content.about.text}
                        </div>
                    </div>;
    let studentNames = content.students .map(student => student.name.first + " " + student.name.last)
                                        .map(name => <div key={name} className={style.tickerItem}>
                                                            {studentNameSeparator}
                                                            <div>{name}</div>
                                                      </div>);

    let students = <div key="students" className={style.tickerContent}>{studentNames}</div>;

    let projectTitles = content.projects.map(p => p.title).map(title => <div key={title} className={style.tickerItem}>
                                                                            {projectTitleSeparator}
                                                                            <div>{title}</div>
                                                                        </div>);
    
    let projects = <div key="projects" className={style.tickerContent}>{projectTitles}</div>;
    let tickerContents = [introText, students, projects];
    

    let remainder = Math.random() > 0.5 ? 1:0;
    let tickers = [ ... Array(6).keys() ].map(i => {
        let content =
            i%2 === remainder ?
                tickerContents.splice(Math.floor(Math.random()*tickerContents.length), 1)[0]
                : null;

        let svgString = process.env.IS_BROWSER ? require('./logos/white-0' + (i+1) + ".svg") : "";
        let logo = <SVGComponent className={style.logo} svgString={svgString}/>;
        
        let rotationDirection = Math.random() > 0.5 ? 1 : -1;
        let rotation = Math.random() > 0.5 || i === 5 ?  Math.random()*8*rotationDirection : 0;

        let tickerHeight= process.env.IS_BROWSER ? window.innerHeight/6 : 0;
        let tickerStyle={
            transform: 'rotate(' +rotation + 'deg)',
            top: tickerHeight*0.8 + i*(process.env.IS_BROWSER ? window.innerHeight/7.2 : 0)
        };

        return <Ticker style={tickerStyle}
                       key={i}
                       logo={logo}
                       rotation={rotation}
                       content={content}
        />;
    });


    let headerStyle = {
        position: 'relative',
        zIndex: 98,
    };
    return  (
        <div>
            <div className={style.background}>
                {generateBackground()}
            </div>
            <div className={style.tickersWrapper}>
                {tickers}
            </div>
            <div className={style.content}>
                <Header style={headerStyle}/>
                <Gallery projects={content.projects}/>
                <Footer visible={true}/>
            </div>
        </div>
    )
}


function generateBackground(){
    let colorClasses = [bg_red, bg_blue, bg_green, bg_yellow];
    let patternString = process.env.IS_BROWSER ? require('./pattern/pattern.svg') : "";

    Array(2).fill().forEach(val => {
        colorClasses.splice(Math.floor(Math.random()*colorClasses.length), 1);
    });

    let backgroundStripes = Array(3).fill().map( (_, i) => {
        if(i%3 == 0){
            return <SVGComponent key={i}
                                 svgString={patternString}
                                 className={style.backgroundStripe}/>
        } else {
            return <div key={i} className={classNames(style.backgroundStripe, colorClasses[i%colorClasses.length])}/>
        }
    });

    Array(Math.floor(Math.random()*5)).fill().forEach(val => {

        backgroundStripes.unshift(backgroundStripes.pop());

    });

    return backgroundStripes;
}