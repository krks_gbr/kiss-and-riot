


import React from 'react';
import style from './style.scss';


const AnimationStates = {


    LOGO_IN: 'logoIn',
    LOGO_OUT_CONTENT_IN: 'logoOut',
    LOGO_BACK: 'logoBack',

};

class Ticker extends React.Component{
    
    componentWillMount(){
        this.state = {animation: AnimationStates.LOGO_IN};
    }
    
    render () {
        
        let {content, logo} = this.props;
        let componentsToRender = (() => {

            switch(this.state.animation){
                case AnimationStates.LOGO_IN:
                    return [
                        <Animatable component={logo}
                                    key="logo_in"
                                    start={process.env.IS_BROWSER ? window.innerWidth : 0}
                                    calcTarget={(start, width, set) => {  set(start-width-1+width/12)}}
                                    completed={() => {
                                            if(content) setTimeout(() => this.setState({animation: AnimationStates.LOGO_OUT_CONTENT_IN}), 2000) }
                                        }
                                    animParams={{durationFactor: 0.001, easing: 'ease-out'}}
                        />
                    ];

                case AnimationStates.LOGO_OUT_CONTENT_IN:
                    return [
                        <Animatable component={logo}
                                    key="logo_out"
                                    start={0}
                                    calcTarget={(start, width, set) => {  set(start-width+1)}}
                                    animParams={{durationFactor: 0.001}}
                                    // completed={() => setTimeout(this.setState({animation: AnimationStates.LOGO_IN}), 500)}
                        />,
                        <Animatable component={content}
                                    key="content"
                                    start={process.env.IS_BROWSER ? window.innerWidth : 0}
                                    calcTarget={(start, width, set) => {  set(-width-1)}}
                                    animParams={{durationFactor: 0.006}}
                                    completed={() => setTimeout( () => this.setState({animation: AnimationStates.LOGO_BACK}), 500)}
                        />
                    ];

                case AnimationStates.LOGO_BACK:
                    return [
                        <Animatable component={logo}
                                    key="logo_back"
                                    start={process.env.IS_BROWSER ? window.innerWidth : 0}
                                    calcTarget={(start, width, set) => {  set(start-width-1)}}
                                    animParams={{durationFactor: 0.001, easing: 'ease-out'}}
                        />
                    ];

            }

        })();
        
        return (
            <div style={this.props.style} className={style.ticker}>
                {componentsToRender}
            </div>
        )
    }
}

export default Ticker;



class Animatable extends React.Component{



    componentWillMount(){
        this.state = { };
    }


    componentDidMount(){

        let animatable = this.refs.animatable;
        this.bounds = animatable.getBoundingClientRect();
        let transitionEvent = whichTransitionEvent();


        animatable.addEventListener(transitionEvent, this.props.completed);
        this.props.calcTarget( this.props.start, this.bounds.width, this.setTarget.bind(this));
    }

    setTarget(target){
        this.setState({target: target});
    }


    render () {
        if(!this.state.target){

            let initialStyle = {
                left: this.props.start,
            };

            return (
                <div style={initialStyle} ref="animatable" className={style.animatable}>
                    {this.props.component}
                </div>
            )
        } else {

            let start = this.props.start;
            let target = this.state.target;

            let duration = Math.floor(Math.abs(start-target)  * this.props.animParams.durationFactor);
            let easing = this.props.animParams.easing || 'linear';
            let animationStyle = {
                left: this.state.target,
                transition: 'left ' + duration + 's ' + easing
            };

            return (
                <div style={animationStyle} className={style.animatable}>{this.props.component}</div>
            )
        }
    }
}



//https://davidwalsh.name/css-animation-callback
function whichTransitionEvent(){
    var t;
    var el = document.createElement('fakeelement');
    var transitions = {
        'transition':'transitionend',
        'OTransition':'oTransitionEnd',
        'MozTransition':'transitionend',
        'WebkitTransition':'webkitTransitionEnd'
    };

    for(t in transitions){
        if( el.style[t] !== undefined ){
            return transitions[t];
        }
    }
}


