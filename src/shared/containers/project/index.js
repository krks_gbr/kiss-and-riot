



import React from 'react';
import style from './style.scss';
import _ from 'lodash';
import {Link} from 'react-router';
import LRImage from '../../components/lazy-responsive-image';

const HOSTURL = "http://kissandriot.today";

const Project = ({projects, routeParams}) => {



    let project = _.find(projects, {slug: routeParams.projectSlug});

    let studentName = project.student.name;
    studentName = studentName.first + " " + studentName.last;

    let imagePaths = project.imagePaths;
    let isIncludeThumb = false;

    try{
        isIncludeThumb = project.images.map(im => im.originalname).includes(project.thumbnail[0].originalname);
    }catch(err){
        console.log('is ignored',err);
    }

    if(imagePaths.length === 0 || !isIncludeThumb ){
        imagePaths = imagePaths.concat([project.thumbnailPaths]);
    }

    let images = imagePaths
                .map((images,i) =><LRImage key={i} images={images} className={style.image} />);


    let mentors = project.mentors.map(mentor =>  <i key={mentor._id}>{mentor.name.first +" "+mentor.name.last}</i>);

    let metaNodes = [
        <div key="mentors">
            <br/><br/>
            <label>Mentors:</label>
            <br/>
            {mentors}
        </div>
    ];

    let urls = [project.webpage, project.student.webpage];
    urls = Array.from(new Set(urls));
    urls = urls.filter(url => url && url.trim() !== "");

    if(urls.length > 0){
        urls = urls
            .map(url => <a key={url} href={url.includes("http") ? url : "//"+url} target="_blank">{url}</a>);
        metaNodes.unshift(<div key="urls">{urls}</div>)
    }

    let projectIndex = projects.findIndex( p => p._id === project._id );
    let nextProjectSlug = projects[(projectIndex+1) % projects.length].slug;
    metaNodes.push(<Link className={style.nextProject} key="nextproject" to={"/projects/" + nextProjectSlug}>Next Project</Link>);

    return (
        <section className={style.project}>

            <header className={style.header}>
                <div className={style.headerContainer}>
                    <span>{project.title}</span>
                    <span><i>{studentName}</i></span>
                </div>
            </header>
            <section className={images.length > 0 ? style.images : style.empty }>
                    {images}
            </section>
            <article className={style.projectInfo}>
                <p className={style.description}>
                    {project.description}
                </p>
                <div className={style.meta}>
                    {metaNodes}
                </div>
            </article>
        </section>
    )
    
};



export default Project;