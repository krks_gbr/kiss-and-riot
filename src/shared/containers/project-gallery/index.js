



import React from 'react';
import style from './style.scss';
import {Link} from 'react-router';
import _ from 'lodash';
import LRImage from '../../components/lazy-responsive-image';

const HOSTURL = "http://kissandriot.today";



const ProjectGallery = ({projects}) => {


    let thumbnailContainers = projects
                                .filter(project => !!project) //some items in the list are undefined which they shouldn't be
                                .map(project => <ThumbnailContainer key={project._id} project={project}/>);

    if(projects.length < 5){
        let exampleProject = _.find(projects, {slug: 'a-juried-show-of-the-local'});
        let fillers = Array.from({length: 5-projects.length}, (v, k) => k)
            .map(k => <ThumbnailContainer key={k} project={exampleProject}/>);
        thumbnailContainers = thumbnailContainers.concat(fillers);
    }
    
    return (

            <section className={style.projectGallery}>
                <div classsName={style.projectGalleryContainer}>
                    {thumbnailContainers}
                </div>
            </section>


    )


};



const ThumbnailContainer  = ({project}) => {

    let studentName = project.student.name;
    studentName = studentName.first +" "+studentName.last;

    return (
        <Link to={'/projects/'+project.slug} className={style.thumbnailContainer}>
            <figure>

                <figcaption className={style.thumbnailHeader}>
                    <div className={style.thumbnailInfo}>
                        <div> {project.title} </div>
                        <div><i>{studentName}</i></div>
                    </div>
                </figcaption>

                {/*
                 //CSS background-image is not recognized by any Microdata parser such as google robots, etc
                 //so we use a meta tag to keep this shit be readble by search engines
                */}

                <meta itemProp="image" content={HOSTURL+project.thumbnailPaths[2].path}></meta>
                <LRImage images={project.thumbnailPaths} className={style.image}/>

            </figure>
        </Link>
    )

};


export default ProjectGallery;