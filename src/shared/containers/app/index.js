



import React from 'react';
import style from './style.scss';

import Header from '../../components/header';
import SideBar from '../../components/sidebar';
import Footer from '../../components/footer';
import HeaderTemp from '../../components/header-temp';


const mobileRegex = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;


function isMobile() {

    if(!process.env.IS_BROWSER) {
        return false;
    }


    if( mobileRegex.test(navigator.userAgent) ) {
        return screen.width<=736;
    }

    let windowWidth = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

    return windowWidth<=500;
}

const App = props => {

    let HeaderToRender = props.location.pathname === "/" ? HeaderTemp : Header;
    let renderFooter = props.location.pathname !== '/';
    let mobile = isMobile();


        return (
        <div className={style.app}>
            <SideBar mobile={mobile}/>
            <HeaderToRender/>
            <section className={style.content}>
                {props.children}
            </section>
            <Footer visible={renderFooter}/>
        </div>
    );

};


export default App;





