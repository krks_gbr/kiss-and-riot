





import React from 'react';
import style from './style.scss';
import _ from 'lodash';
import {Link} from 'react-router';
import SubHeader from '../../components/sub-header';





export default props => {
    
    
    let { deptCoordinators, dreamteam, externalAdvisors, headsOfDept, photographers, sponsors, title, typeface}
        = props.content;

    let dreamTeamLinks = dreamteam.map(student => {

            let project = _.find(props.projects, project => project.student._id == student._id);
            let studentName = student.name.first + " " + student.name.last;
            return project ?
                        <Link to={"/projects/" + project.slug }>
                            {studentName}
                        </Link>
                        :
                        studentName
                ;
    });





    return (
        <div>
            <SubHeader title="Colophon"/>
            <section className={style.wrapper}>
                <List title="sponsors" items={sponsors}/>
                <List title="Kiss and Riot design team" items={dreamTeamLinks}/>
                <div className={style.listWrapper}>
                    <div className={style.listTitle}>External Advisors</div>
                    <div className={style.list}>{externalAdvisors}</div>
                </div>
                <List title="department coordinators" items={deptCoordinators}/>
                <List title="heads of department" items={headsOfDept}/>
                <List title="photographers" items={photographers}/>
            </section>
        </div>
    )

}


const List = ({items, title}) => {
    let listItems = items.map((item, i) => <li key={i}>{item}</li>);

    return (
        <div className={style.listWrapper}>
            <div className={style.listTitle}>{title}</div>
            <ol className={style.list}>{listItems}</ol>
        </div>
    )

};