




import React from 'react';
import style from './style.scss';
import SubHeader from '../../components/sub-header';


export default ({text}) => {

    let paragraphs = text   .split('\n')
                            .filter(p => p.trim() != "")
                            .map((p, i) => <p key={i}> {p} </p>);
    return (

        <article>
            <SubHeader title="Intro"/>
            <div className={style.about}>
                {paragraphs}
            </div>
        </article>

    );

}