



import React from 'react';
import style from './style.scss';
import GMap from 'google-map-react';
import Marker from './marker';
import SubHeader from '../../components/sub-header';
import classNames from 'classnames';

export default  (props) => {

    const mapOptions = {
        panControl: false,
        mapTypeControl: false,
        scrollwheel: false,
        styles: [
            {
                "featureType": "all",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#00ffba"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "invert_lightness": true
                    },
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "weight": "5"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "hue": "#00ffba"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "invert_lightness": true
                    },
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#0f32f3"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ]    };

    const loc = {
        lat: 52.081818,
        lng: 4.319379
    };


    const street = "Prinsessegracht 4";
    const postCode = "2514 AN";
    const city = "Den Haag";

    const phone = "070 315 4777";
    const url = "http://www.kabk.nl/";
    const FBurl = "https://www.facebook.com/kissandriot/";
    const InstaUrl = "https://www.instagram.com/kiss_riot_2016/"


    return (

        <div className={style.contactLoc}>
            <SubHeader title="Contact/Location"/>
            <section className={style.contactInfo}>

                <address className={style.section}>
                    <div className={style.infoHead}>Location:</div>
                    <div>{street}</div>
                    <div>{postCode}</div>
                    <div>{city}</div>
                </address>
                <br/><br/>

                <section className={style.section}>
                    <div className={style.infoHead}>Contact:</div>
                    <div>{phone}</div>
                    <a href={url} target="_blank">kabk.nl</a>
                    <br/><br/>
                    <a href={FBurl} target="_blank">Facebook</a>
                    <br/>
                    <a href={InstaUrl} target="_blank">Instagram</a>
                </section>
                <br/><br/>

                <section className={classNames(style.open,style.section)}>
                    <div className={style.infoHead}>Open:</div>
                    <div>
                        <div>1 July: 4pm-9pm</div>
                        <div className={style.awards}>(opening & awards: 4pm-5pm)</div>
                    </div>
                    <div>2-7 July: 11am-8pm</div>
                </section>

            </section>

            <section className={style.mapContainer}>
                <GMap options={mapOptions} defaultCenter={loc} defaultZoom={10}>
                    <Marker {...loc} text="KABK"/>
                </GMap>
            </section>

        </div>

    );

}

