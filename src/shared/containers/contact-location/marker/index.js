

import React from 'react';
import style from './style.scss';
import SVGComponent from '../../../components/svg-component';



export default () => {
    const svgString  = process.env.IS_BROWSER ? require('./ic_place_black_24px.svg') : "";
    return (
        <div className={style.marker}>
            <SVGComponent svgString={svgString}/>
        </div>
    );
    
    

}