
import App from './containers/app';
import ProjectGallery from './containers/project-gallery';
import Project from './containers/project';
import ContactLoc from './containers/contact-location';
import Intro from './containers/intro';
import Participants from './containers/participants';
import Colophon from './containers/colophon';
import Landing from './containers/landing';
import {Route, IndexRoute} from 'react-router';
import React from 'react';


const Routes = content => {
    shuffle(content.projects);
    shuffle(content.students);
    return (
        <Route path="/" component={ props => <App data={content} {...props} /> }>
            <IndexRoute component={ props => <Landing content={content}/> }/>
            <Route path="/projects" component={ props => <ProjectGallery projects={content.projects} {...props}/>} />
            <Route path="/projects/:projectSlug" component={props => <Project projects={content.projects}  {...props}/> }/>
            <Route path="/contact" component={ ContactLoc }/>
            <Route path="/participants"
                   component={ props=>
                            <Participants   students={content.students} 
                                            teachers={content.teachers} 
                                            projects={content.projects}
                                            {...props}
                             />
                        }
            />
            <Route path="/intro" component={props => <Intro text={content.about.text}  {...props}/> }/>
            <Route path="/colophon" component={props => <Colophon content={content.colophon} projects={content.projects}  {...props}  />}/>
        </Route>
    )
};

export default Routes;


function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}
