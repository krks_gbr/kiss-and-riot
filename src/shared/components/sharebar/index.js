

import React from 'react';
import style from './style.scss';
import classNames from 'classnames';

import {
    ShareButtons,
    ShareCounts,
    generateShareIcon,
} from 'react-share';

const {
    FacebookShareButton,
    GooglePlusShareButton,
    LinkedinShareButton,
    TwitterShareButton,
} = ShareButtons;

const {
    FacebookShareCount,
    GooglePlusShareCount,
    LinkedinShareCount,
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');

const shareUrl = "http://kissandriot.today/";
const title = "KISS AND RIOT";
const iconSize = 30;

class Sharebar extends React.Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){

        this.setState({open: false});

        if(process.env.IS_BROWSER) {

        }

    }

    getClassName(){
        return this.state.open? classNames(style.open,style.buttonsWrapper): style.buttonsWrapper;
    }


    toggle(){
        let self = this;

        if(process.env.IS_BROWSER) {
            setTimeout(()=>{
                self.setState({open: false});
            },7000);
        }

        this.setState({open: !this.state.open});
    }

    render () {

        return (

            <nav class={style.sharebar}>

                <div className={style.shareBarIcon}
                     onClick={this.toggle.bind(this)}
                ></div>

                <div className={this.getClassName()}>
                    <div className={style.shareButtonWrapper}>
                        <FacebookShareButton url={shareUrl} title={title} className={style.shareButton}>
                            <FacebookIcon size={iconSize}/>
                        </FacebookShareButton>
                    </div>

                    <div className={style.shareButtonWrapper}>
                        <TwitterShareButton url={shareUrl} title={title} className={style.shareButton}>
                            <TwitterIcon size={iconSize} />
                        </TwitterShareButton>
                    </div>


                    <div className={style.shareButtonWrapper}>
                        <GooglePlusShareButton url={shareUrl} className={style.shareButton}>
                            <GooglePlusIcon size={iconSize} />
                        </GooglePlusShareButton>

                    </div>

                    <div className={style.shareButtonWrapper}>
                        <LinkedinShareButton url={shareUrl} title={title} className={style.shareButton}>
                            <LinkedinIcon size={iconSize} />
                        </LinkedinShareButton>
                    </div>

                </div>

            </nav>
        )

    }

}

export default Sharebar;