

import React from 'react';
import style from './style.scss';
import {Link} from 'react-router';



class Header extends React.Component{


    render () {

        return (
            <header className={style.header} style={this.props.style}>
                <div className={style.container}>
                    <section className={style.logo}>
                        <Link to="/">KISS AND RIOT</Link>
                    </section>
                    <nav>
                        <Link to="/intro">INTRO</Link>
                        <span className={style.separator}>/</span>
                        <Link to="/projects">PROJECTS</Link>
                        <span className={style.separator}>/</span>
                        <Link to="/participants">PARTICIPANTS</Link>
                    </nav>
                </div>
            </header>
        );

    }

}

export default Header;