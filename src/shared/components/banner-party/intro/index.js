

import React from 'react';
import {chunk} from 'lodash';
import style from './style.scss';
import colors from '../../../style/colors.scss';
import classNames from 'classnames';


let allColors = chunk(Object.keys(colors),6);
let timer = null;
let svgString = null;

class Intro extends React.Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){

        this.state = {
            background: this.getNext(1),
            foreground: this.getNext(0)
        }

        if(process.env.IS_BROWSER) {

            svgString = require('../../../svgs/kissandriot_new.svg');

            if(!this.props.flashing)
                return;

            timer = setInterval(()=>{
                this.setState({
                    background: this.getNext(1),
                    foreground: this.getNext(0),
                });
            },100)
        }

    }

    componentWillUnmount(){
        clearInterval(timer);
    }

    getNext(index){
        let nextColor = allColors[index].shift();
        allColors[index].push(nextColor);
        return nextColor;
    }

    render(){
        
        return (
            <section className={ style.introWrapper }
            >
                <SVGComponent/>
            </section>
        );

    }

};


const SVGComponent = props => {

    let svgElement;

    if(process.env.IS_BROWSER && svgString){
        svgElement = <div className={style.svgWrapper}  dangerouslySetInnerHTML={{ __html: svgString }} />
    } else {
        svgElement = <div></div>
    }

    return svgElement;

};


export default Intro;