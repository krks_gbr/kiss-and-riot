

import React from 'react';
import style from './style.scss';

const Video = (props) => {

    
    return (
        
        <div className={style.videoWrapper}>
            <video id="video" autoplay>
                <source src="video/final.webm" type="video/webm" />
                <source src="video/final.ogv" type="video/ogg" />
                <source src="video/final.mp4" type="video/mp4" />
            </video>
        </div>
    )

};

export default Video;