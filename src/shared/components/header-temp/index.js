

import React from 'react';
import style from './style.scss';
import {Link} from 'react-router';
import SVGComponent from '../svg-component';



class Header extends React.Component{

    render () {
        let logoSvgString = process.env.IS_BROWSER ? require('./kabk_logo.svg') : "";

        return (
            <header className={style.header}>

                <div className={style.container}>
                    <section className={style.title}>
                        GRAPHIC DESIGN GRADUATION SHOW
                    </section>
                    <SVGComponent className={style.logo} svgString={logoSvgString}/>
                </div>
            </header>
        );

    }

}

export default Header;