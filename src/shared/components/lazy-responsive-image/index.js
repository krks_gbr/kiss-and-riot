
import _ from 'lodash';
import LazyLoad from 'react-lazy-load';
import React from 'react';

const LRImage = props => {

    let size = getSize();
    let image = _.find(props.images, {width: size});
    let imageStyle = {
        'backgroundImage': "url('" + image.path + "')",
        'backgroundRepeat': 'no-repeat',
        'backgroundPosition': 'center',
        'backgroundSize': 'cover',
        'height': '100%'
    };


    return(
        <LazyLoad offsetHorizontal={200} debounce={false} throttle={50} className={props.className}>
            <div style={imageStyle}/>
        </LazyLoad>
    )
    
};


function getSize(){

    
    let size = 'lg';

    if(process.env.IS_BROWSER){
        let vWidth = window.innerWidth;
        if(vWidth<1280){
            size = 'md';
        }

        if(vWidth<800){
            size='sm';
        }

        if(vWidth<500){
            size='xs';
        }
    }
    
    return size;
}

export default LRImage;