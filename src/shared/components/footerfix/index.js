

import React from 'react';
import style from './style.scss';
import {Link} from 'react-router';
import classNames from 'classnames';
import Counter from '../counter';

const FooterFix = props => {

    return (
        <footer className={classNames(style.footer, props.className)}>

            <nav className={style.links}>
                <Link to="/contact">CONTACT</Link>
                <Link to="/sponsors">SPONSORS</Link>
                <Link to="/credits">CREDITS</Link>
                <Link to="/subscription">SUBSCRIPTION</Link>
                <Link to="/login">LOGIN</Link>
            </nav>

            <nav className={style.socialMedia}>
                <a href="https://www.facebook.com/KABKgraphicdesign/" target="_blank">Facebook</a>/
                <a href="https://www.instagram.com/kiss_riot_2016/" target="_blank">Instagram</a>
            </nav>

            <Counter show={props.hasCounter}/>

        </footer>
    )
};

export default FooterFix;