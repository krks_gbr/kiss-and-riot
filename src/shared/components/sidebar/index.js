

import React from 'react';
import style from './style.scss';
import classNames from 'classnames';


class Sidebar extends React.Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){
        //dynamic code should only be executed client-side, otherwise there will be errors

        this.setState({collapsed: this.props.mobile});



    }

    getClassName(){
        return this.state.collapsed? classNames(style.collapsed,style.sidebar): style.sidebar;
    }


    toggle(toBeOrNotToBe){
        if(!this.props.mobile)
            return;


        toBeOrNotToBe = typeof toBeOrNotToBe == 'object'? !this.state.collapsed:toBeOrNotToBe;

        this.setState({collapsed: toBeOrNotToBe});
    }

    render () {

        return (
            <section
                className={this.getClassName()}
                onMouseOver={this.toggle.bind(this,false)}
                onMouseOut={this.toggle.bind(this,true)}

                /*handle toggle for touch devices*/
                onMouseDown={this.toggle.bind(this)}
            >

                <date className={style.date}>
                    1-7 JULY 2016
                </date>

                <a href="http://www.kabk.nl/" target="_blank">
                    ROYAL ACADEMY OF ART
                    <br/>
                    THE HAGUE
                </a>

            </section>
        )

    }

}

export default Sidebar;