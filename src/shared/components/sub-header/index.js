


import React from 'react';
import style from './style.scss';


export default props => {
    
    return (
        <div className={style.header}>
            <p>{props.title}</p>
        </div>
    )
    
}