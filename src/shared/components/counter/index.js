

import React from 'react';
import style from './style.scss';
import classNames from 'classnames';
import moment from 'moment';


class Counter extends React.Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){
        let timeRemaining = this.countTimeRemaining();
        this.state = { timeRemaining:  timeRemaining  };

        //dynamic code should only be executed client-side, otherwise there will be errors
        if(process.env.IS_BROWSER){
            this.timer = setInterval(() => {

                let timeRemaining = this.countTimeRemaining();
                this.setState({ timeRemaining:  timeRemaining  });

            }, 1000);
        }
    }

    componentWillUnmount(){
        if(process.env.IS_BROWSER) {
            clearInterval(this.timer);
        }
    }

    countTimeRemaining(){

        let launchDate = "2016-07-20 00:00:00";

        let now = moment();
        let then = moment(launchDate);

        let diff = moment.duration( then.diff(now) );

        return {
            days: diff.days(),
            hours: diff.hours(),
            minutes: diff.minutes(),
            seconds: diff.seconds()
        }

    }

    render(){

        // let hiddenClass = this.props.show === false ? 'hidden' : '';

        return (
            <div className={style.counter}>
                <label className={style.label}>site launch in:</label>
                <div>
                    <div className={style.countWrapper}><span className={classNames(style.count,style.first)}>{this.state.timeRemaining.days}</span><span>days</span></div>
                    <div className={style.countWrapper}><span className={style.count}>{this.state.timeRemaining.hours}</span><span>hours</span></div>
                    <div className={style.countWrapper}><span className={style.count}>{this.state.timeRemaining.minutes}</span><span>minutes</span></div>
                    <div className={style.countWrapper}><span className={style.count}>{this.state.timeRemaining.seconds}</span><span>seconds</span></div>
                </div>
            </div>
        )
    }
};




/*
*
*


*
*
*
* */


export default Counter;