

import React from 'react';
import style from './style.scss';
import {Link} from 'react-router';
import Sharebar from '../../components/sharebar';

const Footer = props => {

    let className = props.visible === true ? style.footer : 'hidden';

    return (
        <footer className={className}>

            <nav className={style.links}>
                <Link to="/contact">CONTACT/LOCATION</Link>
                <Link to="/colophon">COLOPHON</Link>
            </nav>
            <Sharebar/>
        </footer>
    )
};

export default Footer;