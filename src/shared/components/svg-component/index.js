


import ownStyle from './style.scss';
import React from 'react';
import classNames from 'classnames';

export default ({svgString, className, style}) => {

    
    return  process.env.IS_BROWSER ?
        <div style={style}
             className={className ? classNames(ownStyle.svgWrapper, className) : ownStyle.svgWrapper}
             dangerouslySetInnerHTML={{ __html: svgString }}
        />
        :
        <div></div>
        ;


}