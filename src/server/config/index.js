

import path from 'path';

const Config = {};

Config.ROOT_DIR = path.dirname(require.main.filename);
Config.STORAGE_DIR = path.join( Config.ROOT_DIR, process.env.STORAGE_DIR );
Config.IMG_DIR = path.join( Config.STORAGE_DIR, 'imgs');
Config.IMG_DIR_PUBLIC_PATH = '/imgs';
Config.PUBLIC_DIR = path.join( Config.ROOT_DIR, 'dist/public/');


export default Config;