/**
 * This script automatically creates a default Admin user when an
 * empty database is used for the first time. You can use this
 * technique to insert data into any List you have defined.
 *
 * Alternatively, you can export a custom function for the update:
 * module.exports = function(done) { ... }
 */

exports.create = {
    User: [
        {
            'name.first': 'Gabor',
            'name.last': 'Kerekes',
            email: 'krks.gbr@gmail.com',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Amir',
            'name.last': 'Houieh',
            email: 'amir.houieh@gmail.com',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Vera',
            'name.last': 'van de Seyp',
            email: 'vera.van.de.seyp@gmail.com',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Andre',
            'name.last': 'Jiberda',
            email: 'andre.jiberda@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Caitlin',
            'name.last': 'Berner',
            email: 'caitlin.berner@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Caro',
            'name.last': 'The',
            email: 'caro.the@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Caroline',
            'name.last': 'Langendoen',
            email: 'caroline.langendoen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Charlotte',
            'name.last': 'Gramberg',
            email: 'charlotte.gramberg@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Christina',
            'name.last': 'Yarashevich',
            email: 'christina.yarashevich@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Eelke',
            'name.last': 'Veenstra',
            email: 'eelke.veenstra@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Elizaveta',
            'name.last': 'Pritychenko',
            email: 'elizaveta.pritychenko@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Erik',
            'name.last': 'van der Veen',
            email: 'erik.van.der.veen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Eva',
            'name.last': 'van Bemmelen',
            email: 'eva.van.bemmelen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Evelien',
            'name.last': 'Broersen',
            email: 'evelien.broersen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Jana',
            'name.last': 'Blom',
            email: 'jana.blom@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Joan',
            'name.last': 'Scheeve',
            email: 'joan.scheeve@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Joeri',
            'name.last': 'Woudstra',
            email: 'joeri.woudstra@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'John',
            'name.last': 'van der Meule',
            email: 'john.van.der.meule@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Jordan',
            'name.last': 'Reaside',
            email: 'jordan.reaside@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Jorick',
            'name.last': 'de Quaasteniet',
            email: 'jorick.de.quaasteniet@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Joelle',
            'name.last': 'Erkamp',
            email: 'joelle.erkamp@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Karina',
            'name.last': 'Zavidova',
            email: 'karina.zavidova@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Lennart',
            'name.last': 'Hendriksma',
            email: 'lennart.hendriksma@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Lisa',
            'name.last': 'Moret',
            email: 'lisa.moret@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Luc',
            'name.last': 'Eggenhuizen',
            email: 'luc.eggenhuizen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Marlen',
            'name.last': 'Weise',
            email: 'marlen.weise@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Marloes',
            'name.last': 'van den Berg',
            email: 'marloes.van.den.berg@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Martijn',
            'name.last': 'de Heer',
            email: 'martijn.de.heer@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Mike',
            'name.last': 'Kokken',
            email: 'mike.kokken@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Naomi',
            'name.last': 'Naus',
            email: 'naomi.naus@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Octavia',
            'name.last': 'van Horik',
            email: 'octavia.van.horik@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Olena',
            'name.last': 'Shkarupa',
            email: 'olena.shkarupa@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Orphe',
            'name.last': 'Tan-A-Kiam',
            email: 'orphe.tan.a.kiam@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Rafael',
            'name.last': 'Jardim Henneberke',
            email: 'rafael.jardim.henneberke@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Reni',
            'name.last': 'van der Gragt',
            email: 'reni.van.der.gragt@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Sanne',
            'name.last': 'Kloppenburg',
            email: 'sanne.kloppenburg@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Sophie',
            'name.last': 'Neppelenbroek',
            email: 'sophie.neppelenbroek@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Tomas',
            'name.last': 'Komen',
            email: 'tomas.komen@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Valeria',
            'name.last': 'Gay',
            email: 'valeria.gay@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Veerle',
            'name.last': 'Hoefnagels',
            email: 'veerle.hoefnagels@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Viktoryia',
            'name.last': 'Shydlouskaya',
            email: 'viktoryia.shydlouskaya@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Yara',
            'name.last': 'Matias Pombo Crispim Veloso',
            email: 'yara.matias.pombo.crispim.veloso@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Zineb',
            'name.last': 'Benassarou',
            email: 'zineb.benassarou@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },

        //TEACHERS
        {
            'name.first': 'Roosje',
            'name.last': 'Klap',
            email: 'roosje.klap@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Frits',
            'name.last': 'Deys',
            email: 'frits.deys@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Matthias',
            'name.last': 'Kreuzer',
            email: 'matthias.kreuzer@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Ruben',
            'name.last': 'Pater',
            email: 'ruben.pater@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Eric',
            'name.last': 'Schrijver',
            email: 'eric.schrijver@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Dirk',
            'name.last': 'Vis',
            email: 'dirk.vis@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Jan',
            'name.last': 'Robert Leegte',
            email: 'jan.robert.leegte@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        },
        {
            'name.first': 'Michel',
            'name.last': 'Hoogervorst',
            email: 'michel.hoogervorst@kissandriot.today',
            password: 'k1$$aNdR10t',
            isAdmin: true
        }
    ],


    Student: [
        {'name.first': 'Gabor', 'name.last': 'Kerekes'},
        {'name.first': 'Vera', 'name.last': 'van de Seyp'},
        {'name.first': 'Amir', 'name.last': 'Houieh'},
        {'name.first': 'Andre', 'name.last': 'Jiberda'},
        {'name.first': 'Caitlin', 'name.last': 'Berner'},
        {'name.first': 'Caro', 'name.last': 'The'},
        {'name.first': 'Caroline', 'name.last': 'Langendoen'},
        {'name.first': 'Charlotte', 'name.last': 'Gramberg'},
        {'name.first': 'Christina', 'name.last': 'Yarashevich'},
        {'name.first': 'Eelke', 'name.last': 'Veenstra'},
        {'name.first': 'Elizaveta', 'name.last': 'Pritychenko'},
        {'name.first': 'Erik', 'name.last': 'van der Veen'},
        {'name.first': 'Eva', 'name.last': 'van Bemmelen'},
        {'name.first': 'Evelien', 'name.last': 'Broersen'},
        {'name.first': 'Jana', 'name.last': 'Blom'},
        {'name.first': 'Joan', 'name.last': 'Scheeve'},
        {'name.first': 'Joeri', 'name.last': 'Woudstra'},
        {'name.first': 'John', 'name.last': 'van der Meule'},
        {'name.first': 'Jordan', 'name.last': 'Reaside'},
        {'name.first': 'Jorick', 'name.last': 'de Quaasteniet'},
        {'name.first': 'Joelle', 'name.last': 'Erkamp'},
        {'name.first': 'Karina', 'name.last': 'Zavidova'},
        {'name.first': 'Lennart', 'name.last': 'Hendriksma'},
        {'name.first': 'Lisa', 'name.last': 'Moret'},
        {'name.first': 'Luc', 'name.last': 'Eggenhuizen'},
        {'name.first': 'Marlen', 'name.last': 'Weise'},
        {'name.first': 'Marloes', 'name.last': 'van den Berg'},
        {'name.first': 'Martijn', 'name.last': 'de Heer'},
        {'name.first': 'Mike', 'name.last': 'Kokken'},
        {'name.first': 'Naomi', 'name.last': 'Naus'},
        {'name.first': 'Octavia', 'name.last': 'van Horik'},
        {'name.first': 'Olena', 'name.last': 'Shkarupa'},
        {'name.first': 'Orphe', 'name.last': 'Tan-A-Kiam'},
        {'name.first': 'Rafael', 'name.last': 'Jardim Henneberke'},
        {'name.first': 'Reni', 'name.last': 'van der Gragt'},
        {'name.first': 'Sanne', 'name.last': 'Kloppenburg'},
        {'name.first': 'Sophie', 'name.last': 'Neppelenbroek'},
        {'name.first': 'Tomas', 'name.last': 'Komen'},
        {'name.first': 'Valeria', 'name.last': 'Gay'},
        {'name.first': 'Veerle', 'name.last': 'Hoefnagels'},
        {'name.first': 'Viktoryia', 'name.last': 'Shydlouskaya'},
        {
            'name.first': 'Yara',
            'name.last': 'Matias Pombo Crispim Veloso'
        },
        {'name.first': 'Zineb', 'name.last': 'Benassarou'}],


    Teacher: [
        {'name.first': 'Roosje', 'name.last': 'Klap'},
        {'name.first': 'Frits', 'name.last': 'Deys'},
        {'name.first': 'Matthias', 'name.last': 'Kreuzer'},
        {'name.first': 'Ruben', 'name.last': 'Pater'},
        {'name.first': 'Eric', 'name.last': 'Schrijver'},
        {'name.first': 'Dirk', 'name.last': 'Vis'},
        {'name.first': 'Jan', 'name.last': 'Robert Leegte'},
        {'name.first': 'Michel', 'name.last': 'Hoogervorst'}
    ]
};
