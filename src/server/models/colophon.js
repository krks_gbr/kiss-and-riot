



import keystone from 'keystone';


let Types = keystone.Field.Types;
var Colophon = new keystone.List('Colophon', {
    label: 'Colophon ',
    plural: 'Colophon ',
    map: {
        name: 'title'
    }
});


Colophon.add({
    title: {    type: String, default: 'Colophon', noedit: true  },
    sponsors: {type: Types.TextArray},
    externalAdvisors: {type: String },
    headsOfDept: {type: Types.TextArray},
    deptCoordinators: {type: Types.TextArray},
    photographers: {type: Types.TextArray},
    dreamteam: { type: Types.Relationship, ref: 'Student', many: true },
    typeface: {type: String}
});

Colophon.register();
