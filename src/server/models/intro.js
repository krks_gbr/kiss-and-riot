




import keystone from 'keystone';


let Types = keystone.Field.Types;
var Intro = new keystone.List('Intro', {
    label: 'Intro ',
    plural: 'Intro ',
    map: {
        name: 'title'
    }
});


Intro.add({
    title: { type: String, default: 'Intro', noedit: true  },
    text: { type: Types.Textarea, height: 500 }
});


Intro.defaultColumns = 'text';
Intro.register();
