



import keystone from 'keystone';
import ImageManager from '../../lib/ImageManager';
import Config from '../../config';
import path from 'path';


const createProfileModel = modelName => {
    
    let Types = keystone.Field.Types;
    let thumbnailDimensions = ImageManager.Sizes.THUMBNAIL;


    let Profile = new keystone.List(modelName, {
        autokey: {path: 'slug', from: 'name', unique: true }
    });

    let imageDirectoryName = modelName.toLowerCase() + 's';

    Profile.add({
        name:  { type: Types.Name, required: true, index: true},
        email: { type: Types.Email,  index: true},
        webpage:    { type: Types.Url },
        statement:  { type: Types.Textarea, height: 500 },
        photo: {
            type: Types.LocalFiles,
            dest: path.join(Config.IMG_DIR, imageDirectoryName),
            prefix: path.join(Config.IMG_DIR_PUBLIC_PATH, imageDirectoryName),
            format: function (item, file) {
                return '<img src="' + file.href + '" style="max-width: 100px">'
            }
        }
    });



    Profile.schema.virtual('imagePaths').get(function(){
        return this.photo.map(function(img){
            return thumbnailDimensions.widths().map(function(width){
                return {
                    width: width,
                    baseFilename: img.filename,
                    path: img.webpath + "/" + ImageManager.formatFileNameForSize(img, width)
                }
            });
        });
    });

    Profile.schema.post('init', function(){
        this._original = this.toObject();
    });

    Profile.schema.pre('save', function(next){
        if(this._original){
            ImageManager.manage( this._original.photo, this.photo, thumbnailDimensions.dimensions );
        }
        next();
    });

    Profile.schema.pre('remove', function(next){
        if(this.photo){
            ImageManager.removeAll(this.photo);
        }
        next();
    });


    Profile.defaultColumns = 'name, webpage, email';
    Profile.register();
    
};

export default createProfileModel;