


import keystone from 'keystone';
import _ from 'lodash';

export default {
        getContent: () => {
            return new Promise((resolve, reject) => {
                let content = {
                    'projects': getProjects(),
                    'about': getIntroduction(),
                    'colophon': getColophon(),
                    'students': getStudents(),
                    'teachers': getTeachers(),
                    
                };

                let promises = _.values(content);

                // when promise resolves, replace promise with returned content
                Object.keys(content).forEach( k => {
                    content[k].then( result => {
                        content[k] = result;
                    });
                });

                // wait for all promises to resolve
                Promise.all(promises).then(function (r) {

                    resolve(content);

                }).catch(e => console.log('problem fetching content', e));
            });
        }
}

function getColophon(){
    return new Promise((resolve, reject) =>{

        keystone.list('Colophon').model
                .findOne()
                .populate('dreamteam')
                .exec()
                .then(colophon => resolve(colophon))

    });
}

function getIntroduction(){

    return new Promise( (resolve, reject) => 
        keystone.list('Intro')
                .model.findOne()
                .exec()
                .then(info => resolve(info))

    );
    
}

function getStudents(){
    return new Promise((resolve, reject) =>
        keystone.list('Student').model
                .find().exec()
                .then(students => resolve(students))
    );
}

function getTeachers(){
    return new Promise((resolve, reject) =>
        keystone.list('Teacher').model
            .find().exec()
            .then(teachers => resolve(teachers))
    );
}


function getProjects(){
    console.log('getting projects');
    return new Promise((resolve, reject) => {

        keystone.list('Project').model.find()
            .populate('student mentors')
            .exec()
            .then(projects => {
                let result = projects
                    .filter(project =>
                                        // project.images.length > 0 &&
                                        project.thumbnail.length > 0
                    )
                    .map(project => {
                        let p = JSON.parse(JSON.stringify(project));
                        p.thumbnailPaths = project.thumbnailPaths;
                        p.imagePaths = project.imagePaths;
                        return p;
                    });
                resolve(result);
            });
    }).catch(e => console.log('problem fetching projects', e));
}