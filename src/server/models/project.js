




import keystone from 'keystone';
import ImageManager from '../lib/ImageManager';
import Config from '../config';
import path from 'path';


let Types = keystone.Field.Types;
let imageDimensions = ImageManager.Sizes.LARGE;


let Project = new keystone.List('Project', {
    map: { name: 'title' },
    autokey: {path: 'slug', from: 'title', unique: true }
});


Project.add({
    title:  { type: String, required: true, initial: true, index: true},
    student: {type: Types.Relationship, required: true, initial: true, ref: 'Student'},
    mentors: {type: Types.Relationship,  required: true, initial: true, many: true, ref: 'Teacher'},
    webpage:    { type: Types.Url },
    description:        { type: Types.Textarea, height: 500 },
    thumbnail: {
        type: Types.LocalFiles,
        dest: path.join(Config.IMG_DIR, 'projects/thumbnails'),
        prefix: path.join(Config.IMG_DIR_PUBLIC_PATH, 'projects/thumbnails'),
        format: function(item, file){
            return '<img src="' + file.href + '" style="max-width: 100px">'
        }
    },
    images: {
        type: Types.LocalFiles,
        dest: path.join(Config.IMG_DIR, 'projects'),
        prefix: path.join(Config.IMG_DIR_PUBLIC_PATH, 'projects'),
        format: function(item, file){
            return '<img src="' + file.href + '" style="max-width: 100px">'
        }
    }
});

Project.schema.virtual('imagePaths').get(function(){
    return this.images.map(function(img){
        return imageDimensions.widths.map(function(width){
            return {
                width: width,
                baseFilename: img.filename,
                path: img.webpath + "/" + ImageManager.formatFileNameForSize(img, width)
            }
        });
    });
});

Project.schema.virtual('thumbnailPaths').get(function(){
    if (this.thumbnail.length > 0) {
        let thumbnail = this.thumbnail[0];
        return imageDimensions.widths.map(function (width) {
            return {
                width: width,
                baseFilename: thumbnail.filename,
                path: thumbnail.webpath + "/" + ImageManager.formatFileNameForSize(thumbnail, width)
            }
        });
    } else {
        return null;
    }
});

Project.schema.post('init', function(){
    this._original = this.toObject();
});

Project.schema.pre('save', function(next){
    if(this._original){
        ImageManager.manage( this._original.images, this.images, imageDimensions.dimensions );
        ImageManager.manage( this._original.thumbnail, this.thumbnail, imageDimensions.dimensions );
    }
    next();
});

Project.schema.pre('remove', function(next){
    if(this.images){
        ImageManager.removeAll(this.images);
    }
    if(this.thumbnail){
        ImageManager.removeAll(this.thumbnail);
    }
    next();
});


Project.defaultColumns = 'title, student, thumbnail';
Project.register();

