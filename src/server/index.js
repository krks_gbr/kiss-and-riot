

import keystone from 'keystone';
import Config from './config';

let mongoURL = process.env.MONGO_URL;

export default function Server(callback) {

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

	keystone.init({

		'name': 'kissandriot',
		'brand': 'Kiss&Riot',
		'mongo': mongoURL,
		'static': [ Config.STORAGE_DIR,
					Config.PUBLIC_DIR ],
		'favicon': 'client/public/favicon.ico',
		'views': 'views',
		'view engine': 'jade',

		'auto update': true,
		'session': true,
		'auth': true,
		'user model': 'User'

	});

// Load your project's Models

	keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

	keystone.set('locals', {
		_: require('lodash'),
		env: keystone.get('env'),
		utils: keystone.utils,
		editable: keystone.content.editable
	});

// Load your project's Routes

	keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI

	keystone.set('nav', {
		'students': 'students',
		'teachers': 'teachers',
		'projects': 'projects',
		'colophon': 'Colophon',
		'intro': 'Intro'
	});

// Start Keystone to connect to your database and initialise the web server

	keystone.start({
		onStart:  () => {
			if(callback){
				callback();
			}
		}
	});
}
