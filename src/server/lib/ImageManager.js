
var FileUtil = require('./FileUtil');
var _  = require('lodash');
var sharp  = require('sharp');
var path = require('path');
var fs = require('fs');

module.exports = {


    removeAll: function(images){
        images.forEach(function(img){
           FileUtil.removeFiles(img.path, function(fname){
              return fname.indexOf(img.filename) > -1;
           });
        });
    },

    manage: function(prevImages, images, imageDimensions){

        var fileDiff = FileUtil.getFileDiff(prevImages, images);
        
        fileDiff['added'].forEach(function(img){
            createResizedImages(img, imageDimensions);
        });


        fileDiff['removed'].forEach(function(img){
            FileUtil.removeFiles(img.path, function(fname){
                let baseName = path.basename(img.filename, path.extname(img.filename));
                return fname.indexOf(baseName) > -1;
            });
        });
    },
    
    Sizes: {

        LARGE: Dimensions({
                lg: [1920, 1280],
                md: [1280, 853],
                sm: [853, 568],
                xs: [568, 378]
        }),

        THUMBNAIL: Dimensions(

            [400, 400],
            [200, 200]

        )
    },
    
    formatFileNameForSize: formatFileNameForWidth
};

function createResizedImages(img, dimensions, cb){
    var imgPath = path.join(img.path, img.filename);
    let ds = Object.keys(dimensions);
    ds.forEach(function(key, i){

        var dimension = dimensions[key];
        var w = dimension[0];
        var h = dimension[1];
        
        var outfileName = formatFileNameForWidth(img, key);
        var outpath = path.join(img.path, outfileName);

        sharp(imgPath)
            .resize(w, h)
            .max()
            .toFile(outpath, function(err){
                //TODO handle error
                if(err){

                    console.log("SHARP fucked up", err);

                } else if(i === ds.length-1){
                    //create small preview file from original and overwrite original
                    //delay to be safe
                    setTimeout(() => {
                        sharp(outpath)
                            .resize(200, 200)
                            .max()
                            .toFile(imgPath, function(err){
                                if(err){console.log(err);
                                    throw new Error(err);
                                }
                            });
                    }, 5000);
                }
            });
    });
}

function formatFileNameForWidth(img, imgWidth){
    let extname = path.extname(img.filename);
    let baseName = path.basename(img.filename, extname);
    return  baseName + "_" + imgWidth + extname;
}



function Dimensions(d) {
    return {
        dimensions: d,
        widths: Object.keys(d)
    }
}