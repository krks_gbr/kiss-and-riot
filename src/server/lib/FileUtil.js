
var _ = require('lodash');
var fs = require('fs');
var path = require('path');

module.exports = {

    // find the difference between prevfiles and currentfiles 
    getFileDiff: function(prevFiles, currentFiles){
        var getFileName = function(img){   return img.filename  };
        var prevFileNames = prevFiles.map(getFileName);
        var currentFileNames = currentFiles.map(getFileName);
        var removedFileNames = _.difference(prevFileNames, currentFileNames);
        var addedFileNames = _.difference(currentFileNames, prevFileNames);

        return {
            'added':    currentFiles.filter(function(file) { return addedFileNames.indexOf( file.filename ) > -1 }),
            'removed':  prevFiles.filter(function(file){  return removedFileNames.indexOf(  file.filename ) > -1 })
        }
    },

    // removes files from dirpath for which filenamePredicate returns true
    removeFiles: function(dirpath, filenamePredicate){
        fs.readdir(dirpath, function(err, files){
            if(files){
                files
                        .filter(filenamePredicate)
                        .map(function(file){ return path.join(dirpath, file)    })
                        .filter(function(file){
                            try{
                                fs.accessSync(file, fs.F_OK);
                                return true;
                            } catch(e) {
                                console.log(e);
                                return false;
                            }
                        })
                        .filter(function(file){
                            return !fs.statSync( file ).isDirectory()
                        })
                        .forEach(function(file){
                            console.log('removing', file);
                            fs.unlink( file );
                        });
            }
        });
    }
    
};