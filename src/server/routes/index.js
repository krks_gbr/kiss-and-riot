import Model from '../models/model';
import path from 'path';
import ReactDOMServer from 'react-dom/server'
import React from 'react';
import {match, RouterContext} from 'react-router'
import Routes from '../../shared/routes';
import middleWare from './middleware';

let rootPath = path.dirname(require.main.filename);
let publicPath = path.resolve(rootPath, 'dist/public');
let requireUser = middleWare.requireUser;


function handleRedirect(res, redirectLocation) {
    res.redirect(302, redirectLocation.pathname + redirectLocation.search)
}

function handleError(res) {
    console.log('Error', error);
    res.status(500).send(error);
}


function handleRoute(renderProps, res, content) {

    var html = ReactDOMServer.renderToStaticMarkup(
            <RouterContext {...renderProps}/>
    );

    if (process.env.NODE_ENV === 'development') {

        let script = require(path.resolve(rootPath, 'webpack/dev.config.js')).output;
        res.locals.script = `${script.publicPath}${script.filename}`;
        // res.locals.script = `${script.mobilePath}${script.filename}`;


    } else {

        let assets = require(path.resolve(publicPath, 'stats.json'));
        res.locals.script = "/"+assets.main;
        res.locals.css = "/"+assets.css;

    }

    res.locals.data = content;
    res.locals.appHTML = html;
    res.render('index');

}


function handleRequest(req, res){

    Model.getContent().then(content => {
        
        let routes = Routes(content);

        match({ routes, location: req.url }, function (error, redirectLocation, renderProps) {

            if (error) {

                handleError(res, error);

            } else if (redirectLocation) {

                handleRedirect(res, redirectLocation);

            } else if (renderProps) {

                handleRoute(renderProps, res, content);

            } else {

                res.status(404).send('Not found')

            }

        });
    }).catch(e => console.log(e));
}



exports = module.exports = function (app) {
        app.get('*', handleRequest);
};



