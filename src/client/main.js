
import { Router, browserHistory } from 'react-router';
import '../shared/style/global.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import Routes from '../shared/routes';


const routes = Routes(window.__data__);

if(process.env.NODE_ENV === "production"){
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-79554320-1', 'auto');
    ga('send', 'pageview');
}

ReactDOM.render(
    <Router routes={routes}
            history = {browserHistory}
            onUpdate={() => window.scrollTo(0, 0)}
    />
    , document.getElementById('app')
);