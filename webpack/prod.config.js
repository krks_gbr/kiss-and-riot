
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require('webpack');
var StatsWriterPlugin = require("webpack-stats-plugin").StatsWriterPlugin;
var extractCSS = new ExtractTextPlugin("[hash].min.css");
const autoprefixer = require('autoprefixer');



module.exports = {
    entry: {
        main: ["babel-polyfill", path.resolve("./src/client/main.js")],
    },
    output: {
        path: path.resolve("./dist/public"),
        filename: "[hash].js",
    },
    module:{
        loaders:[

            { test: /\.(png|woff|woff2|eot|ttf)$/, loader: 'url-loader?limit=100000' },

            {
                loader: extractCSS.extract('style','css?modules&importLoaders=1&localIdentName=[hash:base64:5]!postcss!resolve-url!sass?sourceMap'),
                test: /\.scss$/,
            },

            {
                test: /\.svg$/,
                loader: 'svg-inline'
            },

            {
                test: /\.css$/,
                loader: extractCSS.extract('style','css?modules&importLoaders=1&localIdentName=[hash:base64:5]!postcss!resolve-url' )
            },

            {
                test: /\.jsx?$/,
                loader: 'babel',
                include: [ path.resolve('./src/client/'), path.resolve('src/shared/') ]
            }
        ]
    },


    postcss: function(){
        return [autoprefixer];
    },


    plugins: [
        extractCSS,
        new webpack.DefinePlugin({
            'process.env': {
                'IS_BROWSER': "'true'",
                'NODE_ENV': "'production'"
            }
        }),

        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),

        // Write out stats file to build directory.
        new StatsWriterPlugin({
            transform: function (data) {
                return JSON.stringify({
                    main: data.assetsByChunkName.main[0],
                    css: data.assetsByChunkName.main[1]
                }, null, 4);
            }
        })
    ]

};