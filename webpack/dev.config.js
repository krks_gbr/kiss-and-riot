
const path = require('path');
const webpack = require('webpack');
const rootDir = path.dirname(require.main.filename);
const host = process.env.HOST || "0.0.0.0";
const port = process.env.WP_DEV_PORT || 3001;
const dist = path.resolve(rootDir, 'dist');
const config = require('../package').config;
const autoprefixer = require('autoprefixer');

module.exports = {
    devtool: "source-map",
    entry: [
        "babel-polyfill",
        `webpack-dev-server/client?http://${host}:${port}`,
        "webpack/hot/dev-server",
        path.resolve(rootDir, 'src/client/main.js')
    ],
    output: {
        path: dist,
        filename: 'bundle.js',
        publicPath: `http://${host}:${port}/dist/`,
        mobilePath: `http://${"192.168.1.67"}:${port}/dist/`
    },

    module:{
        loaders:[

            { test: /\.(png|jpg|woff|woff2|eot|ttf)$/, loader: 'url' },

            {
                loaders: [ 'style', `css?modules&importLoaders=1&localIdentName=${config.css}`, 'postcss', 'resolve-url', 'sass?sourceMap'],
                test: /\.scss$/,
            },

            {
                test: /\.svg$/,
                loader: 'svg-inline'
            },
            
            {
                test: /\.css$/,
                loaders: [ 'style', `css?modules&importLoaders=1&localIdentName=${config.css}`, 'postcss', 'resolve-url' ]
            },

            {
                test: /\.jsx?$/,
                loaders: ['react-hot', "babel"],
                include: [path.resolve(rootDir, 'src/client/'), path.resolve(rootDir, 'src/shared/')]
            }
        ]
    },

    postcss: function(){
        return [autoprefixer];
    },
    
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'IS_BROWSER': "'true'",
                'NODE_ENV': "'development'"
            }
        })
    ]

};