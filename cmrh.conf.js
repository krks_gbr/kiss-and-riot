

var cssRequireHook = require( 'css-modules-require-hook');
var sass = require( 'node-sass');
var config = require('./package').config;

cssRequireHook({
    generateScopedName: config.css,
    extensions: [ '.scss'],
    preprocessCss: function (data, filename) {
        return sass.renderSync({
            data,
            file: filename,
        }).css
    }
});