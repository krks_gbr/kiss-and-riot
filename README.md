

# Branches
There are two branches, the "master" which is the main line of development 
and "prototyping" which obviously is for prototyping. It's better to separate the two
because master is going to become fucking huge over time and difficult to navigate. 
This is how I imagine our roles to be:
Vera prototypes on her branch, without worrying too much about compatibility, mobile, clean code, structure etc.
It's more important to be fast, and try out a lot of things.
Amir and I will implement your prototypes in the master branch, taking care of all those things.

to go on the prototyping branch do this:
1. git clone git@gitlab.com:krks_gbr/kiss-and-riot.git
2. git checkout prototyping
3. when you FIRST PUSH:  git push -u origin prototyping
4. after that you can just do: git push

# Tools
We should use webpack and react to build the frontend. 
It's going to be 3x faster to develop cross-browser compatible and bug-free stuff this way.


# setup
1. `git clone git@gitlab.com:krks_gbr/kiss-and-riot.git`
2. `npm install`
3. create 'storage' directory at project root
3. create .env file at project root, with these contents:

###.env
```
COOKIE_SECRET=some-random-string
STORAGE_DIR=storage
MONGO_URL=mongodb://localhost:27017/kissandriot
HOST=0.0.0.0
PORT=3000
WB_DEV_PORT=3001
```

# Run in dev mode
`npm run dev`

#Build everything
`npm run build`

# Build & Run in prod mode
`npm run prod`

# when already built, run in prod mode
`npm start`